/**************************************************************************
 * @brief Rafraichit une des valeurs selon "etat", évite de rafraichir l'écran
 *        complet ce qui provoquerai un scintillement
 * 
 * @param value nouvelle valeur
 * @param etat 1 pour Delais, 2 pour Intensité
 */
void refreshValue(uint16_t value, uint8_t etat) {
  char text[20];
  
  M5.Lcd.fillRect(0, 45 + 90 * (etat - 1), M5.Lcd.width(), 30, TFT_YELLOW);
  M5.Lcd.setTextColor(COULEUR_TEXT_SELECT);  // Set the font color

  M5.Lcd.setTextDatum(MC_DATUM);    //Set text alignment to center

  if(etat == 1) {
    sprintf(text, "%u ms", value);
  } else if(etat == 2) {
    sprintf(text, "%u", value);
  }

  M5.Lcd.drawString(text, 160, 55 + 90 * (etat - 1), 2); 
}

/**************************************************************************
 * @brief Affiche l'écran complet (hors icones)
 * 
 * @param delai : Valeur pour Délai
 * @param intensite : Valeur pour Intensité
 * @param etat : Représente la valeur sélectionnée :
 *                  0 : Aucun sélectionné
 *                  1 : Délai sélectionné
 *                  2 : Itensité sélectionné
 */
void displayScreen(uint16_t delai, uint8_t intensite, uint8_t etat) {
  char text[20];
  uint16_t colorDelai;
  uint16_t colorIntensite;

  M5.Lcd.fillRect(0, 0, M5.Lcd.width(), M5.Lcd.height() - ICONS_HEIGHT, COULEUR_FOND);

  M5.Lcd.setTextDatum(MC_DATUM);    //Set text alignment to center

  switch(etat) {
    case 0 : 
      colorDelai = COULEUR_TEXTE;
      colorIntensite = COULEUR_TEXTE;
      break;
    
    case 1 :
      M5.Lcd.fillRect(0,0,M5.Lcd.width(),75,COULEUR_SELECTION);
      colorDelai = COULEUR_TEXT_SELECT;
      colorIntensite = COULEUR_TEXTE;
      break;
    
    case 2 :
      M5.Lcd.fillRect(0,90,M5.Lcd.width(),75,COULEUR_SELECTION);
      colorDelai = COULEUR_TEXTE;
      colorIntensite = COULEUR_TEXT_SELECT;
      break;
  }

  M5.Lcd.setTextColor(colorDelai);
  sprintf(text, "%u ms", delai);
  M5.Lcd.drawString("Delai", 160, 25, 2); 
  M5.Lcd.drawString(text, 160, 55, 2); 

  M5.Lcd.setTextColor(colorIntensite);
  sprintf(text, "%u", intensite);
  M5.Lcd.drawString("Intensite", 160, 115, 2); 
  M5.Lcd.drawString(text, 160, 145, 2);   
}

/**************************************************************************
 * @brief Affiche les 3 icones du fichier "incons.h" en bas de l'écran
 *        (un en face de chaque bouton)
 */
void displayIcons() {
  M5.Lcd.fillRect(0, M5.Lcd.height() - ICONS_HEIGHT, M5.Lcd.width(), ICONS_HEIGHT, COULEUR_FOND);

  M5.Lcd.drawXBitmap(20, M5.Lcd.height() - ICONS_HEIGHT, menu_bits, ICONS_WIDTH, ICONS_HEIGHT, COULEUR_ICONS);
  M5.Lcd.drawXBitmap(128, M5.Lcd.height() - ICONS_HEIGHT, down_bits, ICONS_WIDTH, ICONS_HEIGHT, COULEUR_ICONS);
  M5.Lcd.drawXBitmap(230, M5.Lcd.height() - ICONS_HEIGHT, up_bits, ICONS_WIDTH, ICONS_HEIGHT, COULEUR_ICONS);
}